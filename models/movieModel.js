var knex = require('knex')(require('../knexfile'));
const TABLE_NAME = "movies";

var movieModel={
    getAllMovies:function(callback){
        knex.select().from(TABLE_NAME).then((result)=>{
            callback(null,result);
        },(err)=>{
            callback(err.message);
        })
    }
}
module.exports = movieModel;