var express = require('express');
var port = 3000;
var movieModel = require('./models/movieModel')
var app = express();

var movieRouter = express.Router();
app.use(function(req,res,next){
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
movieRouter.route('/movies').get(function(request,response){
    var jsonResponse ={}
    movieModel.getAllMovies(function(error,movies){
        if(error){
            jsonResponse.success = false;
            jsonResponse.error ="Something Went Wrong";
            console.log(error);
        }else{
            jsonResponse.success = true;
            jsonResponse.data = movies;
        }
        response.send(jsonResponse);
    })
})

app.use('/',movieRouter);
app.listen(port,function(){
    console.log("Server is listening on port: "+port);
})